package u05lab.code
import java.util.concurrent.TimeUnit
import scala.collection.{immutable, mutable}
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {

  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime() - startTime, TimeUnit.NANOSECONDS)
    if (!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {

  import PerformanceUtils._

  /* Create scala.collection.LinearSeq */
  val list: immutable.List[Int] = (1 to 1000000).toList
  val listBuffer: mutable.ListBuffer[Int] = ListBuffer[Int]()
  listBuffer.addAll(list)
  /* Create scala.collection.IndexedSeq */
  val vector = immutable.Vector(1 to 1000000)
  val array = immutable.ArraySeq(1 to 1000000)
  val arrayBuffer = mutable.ArrayBuffer(1 to 1000000)

  /* Create scala.collection.Set*/
  val mutableSet = mutable.Set(1 to 1000000)
  val immutableSet = immutable.Set(1 to 1000000)

  /* Create scala.collection.Map*/
  val immutableMap: Map[Int, Int] = (1 to 1000000).map(e => e -> e).toMap
  val mutableMap = mutable.Map().addAll(immutableMap)
  println("Create collection [CREATE]")

  println("Comparison of last operation[READ]:")
  /* Read scala.collection.LinearSeq  */
  measure("Immutable List: last") {
    list.last
  }
  measure("Mutable ListBuffer: last") {
    listBuffer.last
  } /*Best*/

  /* Read scala.collection.IndexedSeq*/
  measure("Immutable Vector: last") {
    vector.last
  }
  measure("Immutable Array: last") {
    array.last
  }
  measure("Mutable ArrayBuffer: last") {
    arrayBuffer.last
  }

  /* Read scala.collection.Set*/
  measure("Mutable Set: last") {
    mutableSet.last
  }
  measure("Immutable Set: last") {
    immutableSet.last
  }

  /*Read scala.collection.Map*/
  measure("Mutable Map: last") {
    mutableMap.last
  }
  measure("Immutable Map: last") {
    immutableMap.last
  }

  println("Comparison of add operation[UPDATE]:")
  /* Add scala.collection.LinearSeq  */
  measure("Immutable List: add ") {
    list.::(5)
  }
  measure("Mutable ListBuffer: add") {
    listBuffer.addOne(5)
  }

  /* Add scala.collection.IndexedSeq*/
  measure("Immutable Vector: add") {
    vector.+:(5)
  }
  measure("Immutable Array: add") {
    array.:+(5)
  }
  measure("Mutable ArrayBuffer: add") {
    arrayBuffer.:+(5)
  }

  /* Add scala.collection.Set*/
  measure("Mutable Set: add") {
    mutableSet.+=(5 to 10)
  }
  measure("Immutable Set: add") {
    immutableSet.++(5 to 10)
  }

  /*Add scala.collection.Map*/
  measure("Mutable Map: add") {
    mutableMap.+=((5, 5))
  }
  measure("Immutable Map: add") {
    immutableMap.++(Map(5 -> 5))
  }

  println("Comparison of add operation[DELETE]:")
  /* Delete scala.collection.LinearSeq  */
  measure("Immutable List: drop ") {
    list.drop(1)
  }
  measure("Mutable ListBuffer: drop") {
    listBuffer.drop(1)
  }

  /* Delete scala.collection.IndexedSeq*/
  measure("Immutable Vector: drop") {
    vector.drop(1)
  }
  measure("Immutable Array: drop") {
    array.drop(1)
  }
  measure("Mutable ArrayBuffer: drop") {
    arrayBuffer.drop(1)
  }

  /* Delete scala.collection.Set*/
  measure("Mutable Set: drop") {
    mutableSet.drop(1)
  }
  measure("Immutable Set: drop") {
    immutableSet.drop(1)
  }

  /*Delete scala.collection.Map*/
  measure("Mutable Map: drop") {
    mutableMap.drop(1)
  }
  measure("Immutable Map: drop") {
    immutableMap.drop(1)
  }
}