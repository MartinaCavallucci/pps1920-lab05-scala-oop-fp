package u05lab.code

import u05lab.code.ExamsManager.Kind.Kind

import scala.collection.mutable

object ExamsManager extends App {

  object Kind extends Enumeration {
    type Kind = Value
    val RETIRED, FAILED, SUCCEEDED = Value
  }

  trait ExamResult {

    def getKind: Kind

    def getEvaluation: Option[Int]

    def cumLaude: Boolean
  }

  trait ExamResultFactory {
    def failed: ExamResult

    def retired: ExamResult

    def succeededCumLaude: ExamResult

    def succeeded(evaluation: Int): ExamResult
  }

  trait ExamManager {
    def createNewCall(call: String)

    def addStudentResult(call: String, student: String, result: ExamResult)

    def getAllStudentsFromCall(call: String): Set[String]

    def getEvaluationsMapFromCall(call: String): Map[String, Int]

    def getResultsMapFromStudent(call: String): Map[String, String]

    def getBestResultFromStudent(student: String): Option[Int]
  }

  object ExamResult {
    def apply(kind: Kind, evaluation: Option[Int], cumLaude: Boolean): ExamResult = ExamResultImpl(kind, evaluation, cumLaude)

    case class ExamResultImpl(kind: Kind, evaluation: Option[Int], cumLaude: Boolean) extends ExamResult {
      override def toString: String = kind match {
        case Kind.SUCCEEDED if (!cumLaude) => "SUCCEEDED(" + evaluation.get + ")"
        case Kind.SUCCEEDED => "SUCCEEDED(30L)"
        case Kind.FAILED => "FAILED"
        case Kind.RETIRED => "RETIRED"
      }

      override def getKind: Kind = kind

      override def getEvaluation: Option[Int] = evaluation
    }

  }

  object ExamResultFactory {
    def apply(): ExamResultFactory = ExamResultFactoryImpl()

    case class ExamResultFactoryImpl() extends ExamResultFactory {

      override def failed: ExamResult = ExamResult(Kind.FAILED, Option.empty, cumLaude = false)

      override def retired: ExamResult = ExamResult(Kind.RETIRED, Option.empty, cumLaude = false)

      override def succeededCumLaude: ExamResult = {
        ExamResult(Kind.SUCCEEDED, Option.apply(30), cumLaude = true)
      }

      override def succeeded(evaluation: Int): ExamResult = {
        if (evaluation < 18 || evaluation > 30) throw new IllegalArgumentException
        ExamResult(Kind.SUCCEEDED, Option.apply(evaluation), cumLaude = false)
      }
    }

  }

  object ExamManager {
    def apply(): ExamManager = ExamManagerImpl()

    case class ExamManagerImpl() extends ExamManager {

      var calls: Map[String, mutable.Map[String, ExamResult]] = Map()

      override def createNewCall(call: String): Unit =
        if (calls.contains(call)) throw new IllegalArgumentException()
        else calls += (call -> mutable.Map[String, ExamResult]())

      override def addStudentResult(call: String, student: String, result: ExamResult): Unit = {
        if (calls(call).contains(student)) throw new IllegalArgumentException()
        calls(call) += (student -> result)
      }

      override def getAllStudentsFromCall(call: String): Set[String] = calls(call).keys.toSet

      override def getEvaluationsMapFromCall(call: String): Map[String, Int] = calls(call)
        .filter(x => x._2.getKind == Kind.SUCCEEDED)
        .map(x => x._1 -> x._2.getEvaluation.get).toMap

      override def getResultsMapFromStudent(student: String): Map[String, String] = calls
        .filter(x => x._2.contains(student)).map(x => x._1 -> x._2(student).toString)

      override def getBestResultFromStudent(student: String): Option[Int] = calls
        .flatMap(x => x._2).filter(x => x._1 == student).map(x => x._2.getEvaluation).max

    }

  }

}
